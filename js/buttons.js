

var buttons = {
    
    initialize : function() {
        buttons.accessCameraBtn = new Button("accessCameraBtn", "access camera");
        buttons.accessCameraBtn.activate(
            {
                type : "click",
                func : function() { accessCamera(); }
            }
        );
        buttons.stopBtn = new Button("stopBtn", "stop program");
        buttons.stopBtn.deactivate(
            {
                type : "click",
                func : function() { stop(); }
            }
        );
        buttons.colorBtn = new Button("colorBtn", "show color display");
        buttons.colorBtn.deactivate(
            {
                type : "click",
                func : function() { content.changeDisplay("colorDisplay"); }
            }
        );
        buttons.snapBtn = new Button("snapBtn", "show screenshot");
        buttons.snapBtn.deactivate(
            {
                type : "click",
                func : function() { content.changeDisplay("canvas"); }
            }
        );
        buttons.videoBtn = new Button("videoBtn", "show video");
        buttons.videoBtn.deactivate(
            {
                type : "click",
                func : function() { content.changeDisplay("video"); }
            }
        );
    }
}

function Button(btnId, activeText) {
	this.id = btnId;
	this.element = document.getElementById(this.id);
    this.active = activeText;
	this.hidden = activeText;
    this.listener = [];
    
	this.activate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        } else {
            this.register(this.listener);
        }
		this.show();
	};
    
    this.deactivate = function(newListener) {
        if(arguments.length == 1) {
            this.register(newListener);
        }
        this.deregister();
		this.hide();
	};

	this.register = function(newListener) {
        this.deregister();
        this.listener = newListener;
		this.element.addEventListener(
                newListener["type"],
                newListener["func"]
		);
	};

	this.deregister = function() {
		this.element.removeEventListener(
                this.listener["type"],
                this.listener["func"]
		);
	};
	
    this.show = function() {
		$("#" + this.id).removeClass("greyout");
        $("#" + this.id).addClass("ui-shadow");
	};
    
    this.hide = function() {
		$("#" + this.id).addClass("greyout");
        $("#" + this.id).removeClass("ui-shadow");
	};

	this.text = function() {
		return this.active;
	};
    
	this.getHiddenText = function() {
		return this.hide;
	};
    
	this.changeName = function(newName) {
		// fadeout out animation to let user know something has changed
		$("#" + this.id).fadeOut();
        $("#" + this.id).html(newName);
		$("#" + this.id).fadeIn();
	};
    
	this.setName = function(newName) {
		$("#" + this.id + " a").text(newName);
	};
    
    this.setHiddenText = function(hiddenText) {
		this.hidden = hiddenText;
	};
    
    this.setActiveText = function(activeText) {
		this.active = activeText;
	};
}