
/* 'video' : html video element */
/* var video : video/camera stream variable */

/* 'canvas' : html canvas element for the snapshot of the video stream  */
/* var canvas : canvas variable */
/* var context : canvas context */

var img = new Image();

var avgR = 0; // red
var avgG = 0; // green
var avgB = 0; // blue

/**
 * Take a snapshot of the video stream
 * and draw it into the displayCanvas.
 */
function snapshot() {
	//// source: http://jsfiddle.net/HfyfX/ ////
	$('video').addClass('offscreen');
	canvas.height = $('video').height();
	canvas.width = $('video').width();
	$('video').removeClass('offscreen');
	////////////////////////////////////////////
	context.drawImage(video, 0, 0);
	img.src = canvas.toDataURL("image/jpeg");
	draw();
}

/**
 *
 */
function draw() {
	img.style.display = 'none';
	// get image data from canvas with camera/video snapshot
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	var data = imageData.data;
	avgR = 0; // red
	avgG = 0; // green
	avgB = 0; // blue
  
    // iterate over all image pixels
	// and calculate average 
	for (var i = 0; i < data.length; i += 4) {
	  avgR += data[i];
	  avgG += data[i + 1];
	  avgB += data[i + 2];
	}	
	var total = data.length / 4;
	// rounding numbers
	avgR = ~~(avgR/total);
	avgG = ~~(avgG/total);
	avgB = ~~(avgB/total);
	colorizeDisplay();
}

/** 
 * the current display the user sees.
 * either the average color, the screenshot
 * of the video stream, or the video stream.
 */
function drawCurrentDisplay() {
	
}

function colorizeDisplay() {
	// set color
	displayCanvas.getContext('2d').fillStyle = "rgb(" + avgR + "," + avgG + "," + avgB + ")";
	// get window size
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	// draw color on canvas
	displayCanvas.getContext('2d').fillRect(0,0,windowWidth,windowHeight);
}

/**
 * variable for the ID of the return variable
 * of 'setInterval' from 'start'. Used to
 * end the timer.
 */
var myVar;

/**
 * start the timer on taking snapshots.
 */
function start() {
	myVar = setInterval(snapshot, 250);
}

/**
 * stop the timer on taking snapshots.
 */
function stop() {
	window.clearInterval(myVar);
}