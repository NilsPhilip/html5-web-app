
/**
 * 
 */
function Display(pId) {
	this.id = pId;
	this.element = document.getElementById(this.id);
	this.hide = function() {
		$("#" + this.id).css('display', 'none');
	};
	this.show = function() {
		$("#" + this.id).css('display', 'block');
	};
}

var colorDisplay = new Display("colorDisplay");
var snapshotDisplay = new Display("canvas");
var videoDisplay = new Display("video");

/**
 * 
 */
var content = {
	/** 
	 * the current display the user sees.
	 * either the average color, the screenshot
	 * of the video stream, or the video stream.
	 */
    currentDisplay : colorDisplay, // default
	displays : [colorDisplay, snapshotDisplay, videoDisplay],

	/**
	 * id : String
	 */
	changeDisplay : function(id) {
		// assert id contains to a display in 'displays'
		if (this.currentDisplay.id != id) {
			this.currentDisplay.hide();
			for (var i = 0; i < this.displays.length; i++) {
				this.currentDisplay = this.displays[i];
				if (this.currentDisplay.id == id) {
					this.currentDisplay.show();
					break;
				}
			}
		}
	}
}