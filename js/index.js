
function initialize()
{
    slider.initialize();
    buttons.initialize();
    cameraAccessListener.initialize();
	/* @see screen.js */
	window.addEventListener(
		"resize",
		screen.update
	);
	
	/* update 'snapshotDisplay' on screen size change */
	screen.register(updateVideoSize);
	
	/* 'content' takes up whole screen */
	standardView();
	/* 'colorDisplay' clickable for fullscreen toggle. */
	colorDisplay.element.addEventListener(
		"click",
		changeStyleSheet
	);
}

// debug output set to true from the beginning.
var debug = true;

// toggle visibility of debug paragraph.
function toggleDebug() {
	debug = !debug;
	
	if (debug) {
		document.getElementById('debug').style.visibility = 'visible';
	} else {
		document.getElementById('debug').style.visibility = 'hidden';
	}
}