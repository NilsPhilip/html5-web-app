
/**
 * singleton for listening to the camera access
 * granted event.
 *
 * element : function(param)
 * status : String of the values { "GRANTED", "DENIED" }
 */
var cameraAccessListener = {
    registeredElements : [],
	register : function(element) {
		this.registeredElements.push(element);
	},
	notify : function(status) {
		for (var i = 0; i < this.registeredElements.length; i++) {
			this.registeredElements[i](status);
		}
	}
};

/**
 * Html video element, which source
 * will be set to the camera/video stream
 * via 'accessCamera'.
 */
var video = document.getElementById('video');

/**
 * Get access to the camera and set the video
 * stream via 'navigator.mediaDevices' or
 * the legacy code 'navigator.getUserMedia'.
 */
function accessCamera() {
	// features we want to get access to,
	// here only the video stream of the camera.
	var mediaConstraints = { video: true };
	
	/* code for access to the camera with mediaDevices */
	if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		navigator.mediaDevices.getUserMedia(
			// get access to video stream
			mediaConstraints
		).then(
			// local media stream in anon function
			function(stream) {
				if ("srcObject" in video) {
					video.srcObject = stream;
				} else {
					// legacy code:
					video.src = window.URL.createObjectURL(stream);
				}
				video.onloadedmetadata = function(e) {
					video.play();
				};
				cameraAccessListener.notify("GRANTED");
			}
		).catch(
			handleError
		);
	}
	/* legacy code with getUserMedia (for older browsers) */
	else {
		navigator.getUserMedia = navigator.getUserMedia ||
                         navigator.webkitGetUserMedia ||
                         navigator.mozGetUserMedia;

		if (navigator.getUserMedia) {
			navigator.getUserMedia(
				mediaConstraints,
				function(stream) {
					if ("srcObject" in video) {
						video.srcObject = stream;
					/* deprecated */
					} else if (navigator.webkitGetUserMedia || navigator.mozGetUserMedia) {
						video.src = window.URL.createObjectURL(stream);
					} else {
						video.src = stream;
					}
					video.onloadedmetadata = function(e) {
						video.play();
					};
					cameraAccessListener.notify("GRANTED");
				},
				handleError
		   );
		} else {
		   handleError("getUserMedia not supported");
		}
	}
}

function handleError(msg) {
	//window.alert(msg);
	console.log(msg);
}

function handleError(error) {
	//window.alert("An error has occurred!");
	console.log("An error has occurred!", error);
}
