
/**
 * window interface for easy access
 * to updated screen size properties
 * without the scrollbar.
 */
var screen = {
	registeredElements : [],
	width : $(window).width(),
	height : $(window).height(),
	/**
	 * 
	 */
	update : function() {
		/* excludes scrollbar */
		this.width = $(window).width();
		this.height = $(window).height();
		this.notify();
	},
	register : function(element) {
		this.registeredElements.push(element);
	},
	notify : function() {
		for (var i = 0; i < this.registeredElements.length; i++) {
			this.registeredElements[i](this.width, this.height);
		}
	}
}

/**
 * default: display and navigation take up whole screen.
 */
function standardView() {
	window.removeEventListener("resize", function(){
		document.getElementById("content").width = screen.width;
		document.getElementById("content").height = screen.height;
	});
	window.addEventListener("resize", function(){
		document.getElementById("main").width = screen.width;
		document.getElementById("main").height = screen.height;
	});
}

/**
 * fullscreen: display takes up whole screen.
 */
function fullscreen() {
	window.removeEventListener("resize", function(){
		document.getElementById("main").width = screen.width;
		document.getElementById("main").height = screen.height;
	});
	window.addEventListener("resize", function(){
		document.getElementById("content").width = screen.width;
		document.getElementById("content").height = screen.height;
	});
}

//// source: http://uberiquity.com/linked_articles/runnable/swapcss/changecss.html ////
var gCurThemeName = "index";
var newThemeName = "fullscreen";

function changeStyleSheet() {
	removeExternalStyleSheetLink(gCurThemeName);
	if (newThemeName == "fullscreen") {
		fullscreen();
	} else {
		standardView();
	}
	createExternalStyleSheetLink(newThemeName, "css/" + newThemeName + ".css");
	var temp = newThemeName;
	newThemeName = gCurThemeName;
	gCurThemeName = temp;
}

function removeExternalStyleSheetLink(cssLinkId) {
	var cssLink = document.getElementById(cssLinkId);
	cssLink.parentNode.removeChild(cssLink);
}

function createExternalStyleSheetLink(cssLinkId, href) {
	var cssLink = document.createElement("link");
	cssLink.id = cssLinkId;
	cssLink.type = "text/css";
	cssLink.rel = "stylesheet";
	cssLink.href = href;
	cssLink.media = "all";
	document.getElementsByTagName("head")[0].appendChild(cssLink);
}

///////////////////////////////////////////////////////////////////////////////////////
