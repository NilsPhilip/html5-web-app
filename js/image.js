
/**
 * singleton for calculating the average
 * color of the snapshot to be displayed on the screen.
 */
var colorImage = {
    img : new Image(),
	avgR : 0, // red
	avgG : 0, // green
	avgB : 0, // blue
	setRgb : function(red, green, blue) {
		this.avgR = red;
		this.avgG = green;
		this.avgB = blue;
	},
	calcAverage : function(total) {
		// average and rounding
		this.avgR = ~~(this.avgR/total);
		this.avgG = ~~(this.avgG/total);
		this.avgB = ~~(this.avgB/total);
	},
	getRgb : function() {
		return "rgb(" + this.avgR + "," + this.avgG + "," + this.avgB + ")";
	}
}

/* image of mime media type jpeg */
colorImage.img.src = snapshotDisplay.element.toDataURL("image/jpeg");
/* do not show the image */
colorImage.img.style.display = 'none';

/* 'canvas' : html canvas element for the snapshot of the video stream  */
var canvas = snapshotDisplay.element;
/* context of the color display for easier access */
var context = snapshotDisplay.element.getContext('2d');
/* 'video' : html video element */
/* var video : video/camera stream variable */

var videoWidth;
var videoHeight;

/**
 * 
 */
function updateVideoSize() {
	// source: http://jsfiddle.net/HfyfX/
	$('video').addClass('offscreen');
	videoWidth = $('video').width();
	videoHeight = $('video').height();
	$('video').removeClass('offscreen');
}
updateVideoSize();

/**
 * 
 */
function updateColorDisplay() {
	snapshot();
	getAverageColor();
	colorizeDisplay();
}

var stdSnapHeight = canvas.height;
var stdSnapWidth = canvas.width;

/**
 * Take a snapshot of the video stream
 * and draw it into the 'displayCanvas'.
 */
function snapshot() {
	canvas.width = videoWidth;
	canvas.height = videoHeight;
	context.drawImage(video, 0, 0);
}

/**
 * Iterate over the image data and get the average
 * color.
 */
function getAverageColor() {
	// get image data from canvas with camera/video snapshot
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	var data = imageData.data;
	var color = [0, 0, 0]; // red, green, blue
	/* iterate over all image pixels and calculate the average color */
	for (var i = 0; i < data.length; i += 4) {
		for (var j = 0; j < color.length; j++) {
			color[j] += data[i + j];
		}
	}	
	colorImage.setRgb(color[0], color[1], color[2]);
	colorImage.calcAverage(data.length / 4);
}

/* for easier access */
var colorDisplayContext = colorDisplay.element.getContext('2d');

/**
 * draw a rectangle onto the 'colorDisplay'.
 */
function colorizeDisplay() {
	// set color
	colorDisplayContext.fillStyle = colorImage.getRgb();
	// draw color on canvas
	colorDisplayContext.fillRect(0,0,screen.width,screen.height);
}

/**
 * variable for the ID of the return variable
 * of 'setInterval' from 'start'. Used to
 * end the timer.
 */
var started = false;
var myVar;

/**
 * start the timer on taking snapshots.
 */
function start() {
	myVar = setInterval(updateColorDisplay, slider.secondsPerFrame);
	started = true;
}

/**
 * update timer if it started and the seconds per frame changed.
 */
function update() {
	if (started) {
		window.clearInterval(myVar);
		myVar = setInterval(updateColorDisplay, slider.secondsPerFrame);
	}
}

/**
 * stop the timer on taking snapshots.
 */
function stop() {
	window.clearInterval(myVar);
	started = false;
}