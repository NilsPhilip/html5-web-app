

var slider = {
    
    htmlLabel : "#slider-frames-label",
    
    htmlInput : "#slider-frames-input",
    
    sliderText : "Frames Per Second: ",
    
    secondsPerFrame : 250,
    
    initialize : function() {
        $(slider.htmlLabel).html(slider.sliderText + $(slider.htmlInput).val());
        $(document).on(
            'change',
            '#slider-frames',
            function() {
                $(slider.htmlLabel).html(slider.sliderText + $(slider.htmlInput).val());
                slider.secondsPerFrame = Math.round(1000/$(slider.htmlInput).val());
                update();
            }
        );
    }
}